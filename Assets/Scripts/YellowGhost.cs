﻿using UnityEngine;
using System.Collections;

public class YellowGhost : Ghost {
	void Awake(){
		life = 1f;
		score = 50;
		this.GetComponent<Renderer> ().material.color = new Color (1, 0.85f, 0.2f);
	}
	// Use this for initialization
	void Start () {
		Destroy (GameObject.Find ("Holder(Clone)"), life);
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
