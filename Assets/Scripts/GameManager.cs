﻿using UnityEngine;
using System.Collections;

public class GameManager : MonoBehaviour {

	private GameObject[] spawnPoints; 
	public GameObject ghostPrefab;
	
	void Start () {
		spawnPoints = GameObject.FindGameObjectsWithTag("spawnpoint") as GameObject[];
		startGame ();
	}
	
	private bool checkForGhost() {
		/*pseudo code
		 * is there a ghost type on screen, return true
		 * or return false
		 */
		if (GameObject.Find ("Holder(Clone)") == null) {
			return false;
		} else {
			return true;
		}
	}

	public void addNewGhost() {
		/*pseudo code
		 * pick on eof the spawn points
		 * pick a type of ghost
		 * add this to the screen
		 */
		int spawn = Random.Range (0, 9);
		int ghostType = Random.Range (1, 3);
		GameObject ghost = Instantiate (ghostPrefab, spawnPoints[spawn].transform.position, Quaternion.identity) as GameObject;

		switch (ghostType) {
		case 1:
			ghost.AddComponent<RedGhost>();
			Debug.Log("case 1");
		break;
		case 2:
			ghost.AddComponent<YellowGhost>();;
			Debug.Log("case 2");
		break;
		case 3:
			ghost.AddComponent<RedGhost>();;
			Debug.Log("case 3");
		break;
		}
	}

	private void doEverySecond() {
		/*pseudo code
		 * check if checkForGhost() true or false, if not then call addNewGhost()
		 * change time value
		 * check when time is 0
		 * if time isn't 0 then need to call Invoke("doEverySecond", 1.0f); again
		 */
		if (checkForGhost() == false) {
			addNewGhost ();
			Debug.Log ("Ghost Spawned");
		}
		//Debug.Log ("called the method do every second");
		Invoke ("doEverySecond", 1.0f);
	}

	public void startGame(){
		Invoke("doEverySecond", 1.0f);
	}
}
